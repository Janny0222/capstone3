import './App.css';
import './Font.css';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes } from 'react-router-dom';
import { UserProvider } from './UserContext.js';
import { CartContext } from './CartContext.js';
import { useState, useEffect} from 'react';
import { Container } from 'react-bootstrap';
import Home from './pages/Home.js';
import Profile from './pages/Profile.js';
import Logout from './pages/Logout.js';
import AppNavbar from './components/AppNavbar.js';
import User from './pages/User.js';
import Products from './pages/Products.js';
import ProductsMobile from './pages/ProductsMobile.js';
import ProductsPc from './pages/ProductsPc.js';
import ProductsWatch from './pages/ProductsWatch.js';
import ProductsTablet from './pages/ProductsTablet.js';
import ProductsAirPod from './pages/ProductsAirPod.js';
import ProductsAccessories from './pages/ProductsAccessories.js';
import ProductItem from './pages/ProductItem.js';
import AddProduct from './pages/AddProduct.js';
import Order from './pages/Order.js'
import Catalog from './pages/Catalog.js'
import NotFound from './components/NotFound.js';


function App() {
  const [user, setUser] = useState({
    id: null,
  isAdmin: null,
  firstName: null
  });
  const unsetUser = () => {
    localStorage.clear();
  }
  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/api/users/userProfile`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({
        id: localStorage.getItem('userId')
      })
    }).then(response => response.json()).then(result => {
      if(typeof result._id !== 'undefined'){
        // even if we refresh the browser the 'user' state will still have its value re-assigned
        setUser({
          id: result._id,
          isAdmin: result.isAdmin,
          firstName: result.firstName
        })
      }else {
        setUser({
          id: null,
          isAdmin: null,
          firstName: null
        })
      }
    })
  }, [])
  return (
    <>
      <UserProvider value={{user, setUser, unsetUser}}>
        <Router>
          <AppNavbar/>
            <Container fluid>
              <Routes>
                <Route path='/' element={<Home/>} />
                <Route path='/profile' element={<Profile/>} />
                <Route path='/user' element={<User/>} />
                <Route path='/products/add' element={<AddProduct/>} />
                <Route path='/logout' element={<Logout/>} />
                <Route path='/products' element={<Products/>} />
                <Route path='/catalog' element={<Catalog/>} />
                <Route path='/catalog/mobile' element={<ProductsMobile/>}/>
                <Route path='/catalog/pc' element={<ProductsPc/>} />
                <Route path='/catalog/watch' element={<ProductsWatch/>} />
                <Route path='/catalog/tablet' element={<ProductsTablet/>} />
                <Route path='/catalog/airpods' element={<ProductsAirPod/>} />
                <Route path='/catalog/accessories' element={<ProductsAccessories/>} />
                <Route path='/cart' element={<Order/>} />
                <Route path='/products/:productId' element={<ProductItem/>} />
                <Route path='*' element={<NotFound/>} />

              </Routes>
          </Container>
        </Router>
      </UserProvider>
    </>
  );
}

export default App;
