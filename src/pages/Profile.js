import { Row, Col, Form, Container} from 'react-bootstrap'
import UserContext from '../UserContext.js'
import { Navigate } from 'react-router-dom';
import { useContext, useState, useEffect } from 'react';

export default function Profile () {
	const {user} = useContext(UserContext);
	const [profile, setProfile] = useState({})
	const [orderHistory, setOrderHistory] = useState([])

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/api/users/userProfile`, {
			method: 'POST',
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`,
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				id: user.id
			})
		})
		.then(response => response.json())
		.then(result => {
				if(typeof result._id !== 'undefined'){
				setProfile(result)
			}
		})
	}, [])

	useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/api/orders`, {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then(response => response.json())
      .then(result => {
        const paidOrders = result.filter(order => order.isPaid === true);
        setOrderHistory(paidOrders);
      })
      .catch(error => {
        console.error('Error Fetching Paid Orders', error);
      });
  }, []);

	return (
	(user.id !== null)?
	<>
	<Container>
		<Row>
			<div className="">
				<Col>
					<h1 className ="my-1">Profile</h1>
						<hr></hr>
							<h5 className ="my-4">{`${profile.firstName} ${profile.lastName}`}</h5>
							<h5>Email: {profile.email}</h5>
							<h5>Age: {profile.age}</h5>
							<h5>Mobile no: {profile.mobileNo}</h5>
				</Col>
			</div>
		</Row>
		<hr></hr>
		<Row>
			<div className="form-groups">
					<h1 className ="mb-5">Address</h1>
					<Form>
					<Col lg={4} md={6} sm={12}>
						<div className='field-holder'>	
						<input type="text" id="street" className='form-text' required></input>
						<label for="street" className='form-labels'>Street / House no. / Lot no.</label>
						</div>
						</Col>
						<Col lg={4} md={6} sm={12}>
						<div className='field-holder'>
						<input type="text" id="barangay" className='form-text' required></input>
						<label for="barangay" className='form-labels'>Barangay / Subdivision</label>
						</div>
						</Col>
					</Form>
			</div>
		</Row>
	</Container>
	</>
	:
	<Navigate to = '/'/>
	);
}