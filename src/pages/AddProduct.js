import {useState, useContext} from 'react';
import {Button, Form, Container, Col, Row} from 'react-bootstrap';
import {Navigate, useNavigate} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function AddCourse(){

	const navigate = useNavigate();
	const {user} = useContext(UserContext);

	const [productName, setProductName] = useState("");
	const [description, setDescription] = useState("");
	const [quantity, setQuantity] = useState(0);
	const [price, setPrice] = useState(0);
	const [catalog, setCatalog] = useState("");

	function createProduct(event){
		event.preventDefault();

		let token = localStorage.getItem('token');

		fetch(`${process.env.REACT_APP_API_URL}/api/products/create-product`,{
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${token}`
			},
			body: JSON.stringify({
				productName: productName,
				description: description,
        quantity: quantity,
				price: price,
        catalog: catalog
			})
		}).then(response => response.json()).then(result => {
			if(result){
				Swal.fire({
					title: "New Products Added",
					icon: 'success'
				})
				// For Clearing the form
				setProductName('')
				setDescription('')
				setQuantity(0)
				setPrice(0)
        setCatalog('')
				navigate('/products')
			} else {
				Swal.fire({
					title: "Something went wrong",
					icon: 'error'
				})
			}
		})
	}

	return(
		(user.isAdmin === true)?
			<>
				<section>
					<Container>
						<div  className="border-1">
							<h1 className="my-5 text-center">Add Product</h1>
															<Form  onSubmit={event => createProduct(event)}>
																	<Form.Group>
																			<Form.Label>Name:</Form.Label>
																			<Form.Control type="text" placeholder="Enter Product Name" required value={productName} onChange={event => {setProductName(event.target.value)}}/>
																	</Form.Group>
																	<Form.Group>
																			<Form.Label>Description:</Form.Label>
																			<Form.Control type="text" placeholder="Enter Description" required value={description} onChange={event => {setDescription(event.target.value)}}/>
																	</Form.Group>
																						<Form.Group>
																			<Form.Label>Quantity</Form.Label>
																			<Form.Control type="text" placeholder="Enter Quantity" required value={quantity} onChange={event => {setQuantity(event.target.value)}}/>
																	</Form.Group>
																	<Form.Group>
																			<Form.Label>Price:</Form.Label>
																			<Form.Control type="number" placeholder="Enter Price" required value={price} onChange={event => {setPrice(event.target.value)}}/>
																	</Form.Group>
																						<Form.Group>
																			<Form.Label>Catalog</Form.Label>
																			<Form.Control as="select" value={catalog}  onChange={event => {setCatalog(event.target.value)}}>
																									<option value="">Select Catalog</option>
																									<option value="PC">PC</option>
																									<option value="Mobile">Mobile</option>
																									<option value="Tablet">Tablet</option>
																									<option value="Watch">Watch</option>
																									<option value="AirPods">AirPods</option>
																									<option value="Accessories">Accessories</option>
							
																								</Form.Control>
																	</Form.Group>
																	<Button variant="primary" type="submit" className="my-5">Submit</Button>
															</Form>
						</div>
					</Container>
				</section>
			</>
		:
		<Navigate to='/course' />
	)
	
}