import { Container } from 'react-bootstrap';
import Hero from '../components/Hero.js';
import Hero2 from '../components/Hero2.js';


export default function Home(){
	return (
		<>
		<Container>
		<Hero/>
			<Hero2/>
		</Container>
		</>
	)
}