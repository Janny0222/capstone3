import { useContext, useEffect, useState } from 'react';
import OrderView from '../components/OrderView.js'
import UserContext from '../UserContext.js';
import OrderContext from '../OrderContext.js';

export default function Order(){
	const [orders, setOrders] = useState([])
	const {user} = useContext(UserContext)

	let token = localStorage.getItem('token');
	const fetchOrder = () => {
		fetch(`${process.env.REACT_APP_API_URL}/api/orders/`,{
			method: 'GET',
			headers: {
			'Content-Type': 'application/json',
			Authorization: `Bearer ${token}`
		}
		}).then(response => response.json()).then(result => {
			setOrders(result)
		})
		
		
	}
	useEffect(() =>  {
		fetchOrder()
	}, [])

	return(
		<>
		{
			(user.userId !== null)?
			<OrderView orderedData={orders} fetchOrder={fetchOrder}/>
			:
			<h1>You need to Login first to check</h1>
		}
		</>
					
	)
}