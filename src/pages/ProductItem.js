import { useState, useEffect, useContext } from 'react';
import { Container, Button, Form, Col, Card, Row, Figure } from 'react-bootstrap';
import { useParams, Link, useNavigate, Navigate } from 'react-router-dom'
import  UserContext  from '../UserContext.js'
import { useCart } from '../CartContext.js'
import Swal from 'sweetalert2'
import Iphone from '../assets/img/add-to-cart-iphone.png'
import Pc from '../assets/img/add-to-cart-pc.png'
import Watch from '../assets/img/add-to-cart-watch.png'
import Ipad from '../assets/img/add-to-cart-ipad.png'
import AirPods from '../assets/img/add-to-cart-airpods.png'
import Accessories from '../assets/img/add-to-cart-accessories.png'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {faCartPlus, faArrowLeft} from '@fortawesome/free-solid-svg-icons';

export default function ProductItem(){
	const {cartItemCount, setCartItemCount } = useCart();
	const{productId} = useParams()

	const{user} = useContext(UserContext);


	const navigate = useNavigate()
	const [productName, setProductName] = useState("");
	const [description, setDescription] = useState ("");
	const [checkQuantity, setCheckQuantity] = useState('');
	const [catalog, setCatalog] = useState('');
	const [quantity, setQuantity] = useState (0);
	const [price, setPrice] = useState(0);
	const [image, setImage] = useState([]);

	const product = (id) => {
		fetch(`${process.env.REACT_APP_API_URL}/api/orders/create-order`,{
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				products: [{
					productId: id,
					productName: productName,
					quantity: quantity
				}]
			})
		}).then(response => response.json()).then(result => {
			if(result.message === 'Thank you for your purchase!'){
				Swal.fire({
					title: 'Succesful!',
					text: result.message,
					icon: 'success',
					showCancelButton: true,
					confirmButtonText:'Go to cart page!',
					cancelButtonText: 'Close',
				}).then((result =>{
					if(result.isConfirmed){
						fetchCartData();
						setQuantity(0);
				navigate('/cart')
					} else {
						fetchCartData();
						setQuantity(0);
					}
				}))
			} else {
				Swal.fire({
					title: "Something went wrong",
					text: result.message,
					icon: 'error'
				})
			}
		})
	}

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/api/products/${productId}`)
		.then(response => response.json())
		.then(result => {
			setProductName(result.productName)
			setDescription(result.description)
			setPrice(result.price)
			setCheckQuantity(result.quantity);
			setCatalog(result.catalog)
		})
	}, [productId])
	
		useEffect(() => {
			fetchCartData();
		}, []);
		// For Cart
		const fetchCartData = () => {
			fetch(`${process.env.REACT_APP_API_URL}/api/orders`, {
				method: 'GET',
				headers: {
					Authorization: `Bearer ${localStorage.getItem('token')}`
				}
			}).then(response => response.json()).then(result => {
				const itemCount = result.filter(order => !order.isPaid).length;
				setCartItemCount(itemCount)
			}).catch(error => {
				console.error('Error Fetching Cart Data', error)
			});
		};
	

		const goBack = () =>{
			window.history.back();
		}

		const incrementQuantity = () => {
			setQuantity(previousQuantity => Math.min(previousQuantity + 1, checkQuantity))
		}
		
		const decrementQuantity = () => {
			setQuantity(previousQuantity => Math.max(previousQuantity - 1, 0))
		}

		const photoView = () => {
			if(catalog === 'Mobile'){
			return (
				<img src={Iphone} className='img-fluid d-block img-order' />
			)
			} else if (catalog === 'PC') {
				return (
				<img src={Pc} className='img-fluid d-block img-order'/>
				)
			} else if (catalog === 'Watch'){
				return (
				<img src={Watch} className='img-fluid d-block img-order'/>
				)
			} else if (catalog === 'Tablet'){
				return (
				<img src={Ipad} className='img-fluid d-block img-order'/>
				)
			} else if (catalog === 'AirPods'){
				return (
				<img src={AirPods} className='img-fluid d-block img-order'/>
				)
			} else if (catalog === 'Accessories'){
				return (
				<img src={Accessories} className='img-fluid d-block img-order'/>
				)
			}
			
			
		}

	return(
		
		<Container className="mt-5">
		<Button variant='dark' className='text-white' onClick={goBack}>
		<FontAwesomeIcon icon={faArrowLeft}/>
		Go Back</Button>
		<Row className='p-1'>
			<Col md={6} sm={12} >
				<div className='py-1'>
					{photoView()}
				</div>
			</Col>
			<Col md={6} sm={12}>
			<div>
				<Figure>
					<figure>
						<h3 className='m-0'>{productName}</h3>
						<div className='d-flex'>
							<span className='mx-1'>{checkQuantity >= 0 ? 'In stock' : 'No Available'}</span>
							<span className='mx-1'>{catalog === 'Mobile' ? 'Mobile' : catalog === 'PC' ? 'PC' : catalog === 'watch' ? 'Watch' : 'AirPods'}</span>
						</div>
						<div className='py-1'>
							<h6 className='m-0'>Specification:</h6><span>{description}</span>
						</div>
						<h6>&#8369;{price}</h6>
							
								<Form.Group>
								<Form.Label>Quantity:</Form.Label>
								<div className='d-flex'>
										<Form.Control
											type="text"
											min={0}
											max={checkQuantity}
											required
											value={quantity}
											onChange={event => {setQuantity(event.target.value)}}
											/>
								<Button className='ms-1' variant="dark" onClick={incrementQuantity}>+</Button>
								<Button className='ms-1' variant="dark" onClick={decrementQuantity}>-</Button>
								</div>
								</Form.Group>
								
							
							{(user.id !== null)?
									user.isAdmin == true ?
									<Navigate to='/'/>
							:
							<Button className='m-1 p-1 button-purchase' variant="success" onClick={() => product(productId)}>
							<FontAwesomeIcon className='mx-1' icon={faCartPlus}/>
							Add to Cart</Button>
							:
							<Link className="btn btn-danger btn-block" to='/'>Login first to Order</Link>
							}
					</figure>
				</Figure>
			</div>
			</Col>
		</Row>
			
		</Container>
	)
}