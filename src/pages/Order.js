import { useContext, useEffect, useState } from 'react';
import OrderView from '../components/OrderView.js'
import AdminView from '../components/AdminView.js'
import UserContext from '../UserContext.js';
import { useCart } from '../CartContext.js'

export default function Order(){
	const [orders, setOrders] = useState([])
	const {user} = useContext(UserContext)
	const {cartItemCount, setCartItemCount } = useCart();

	let token = localStorage.getItem('token');
	const fetchOrder = () => {
		fetch(`${process.env.REACT_APP_API_URL}/api/orders/`,{
			method: 'GET',
			headers: {
			'Content-Type': 'application/json',
			Authorization: `Bearer ${token}`
		}
		}).then(response =>  response.json()).then(result => {
			setOrders(result)
		})
		
		
	}
	useEffect(() =>  {
		fetchOrder()
	}, [])

	const fetchCartData = () => {
		fetch(`${process.env.REACT_APP_API_URL}/api/orders`, {
			method: 'GET',
			headers: {
				Authorization: `Bearer ${token}`
			}
		}).then(response => response.json()).then(result => {
			console.log(result)
			const itemCount = result.filter(order => !order.isPaid).length;
			setCartItemCount(itemCount)
			console.log(itemCount)
		}).catch(error => {
			console.error('Error Fetching Cart Data', error)
		});
	};
	useEffect(() => {
		fetchCartData();
	}, []);

	return(
		<>
		{
			(user.userId !== null && !user.isAdmin)?
			<OrderView orderedData={orders} fetchOrder={fetchOrder}  fetchCartData={fetchCartData}/>
			:
			<AdminView orderedData={orders} fetchCartData={fetchOrder} />
		}
		</>
					
	)
}