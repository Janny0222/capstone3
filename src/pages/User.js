import { useContext, useEffect, useState } from 'react';
import AdminView from '../components/AdminView.js'
import UserView from '../components/UserView.js'
import UserContext from '../UserContext.js';

export default function Products(){
	const [users, setUsers] = useState([])
  const {user} = useContext(UserContext)

  let token = localStorage.getItem('token');
	const fetchUsers = () => {
		fetch(`${process.env.REACT_APP_API_URL}/api/users/AllUser`,{
      method: 'GET',
      headers:{
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`
      }
    })
		.then(response => response.json())
		.then(result => {
			setUsers(result)
      console.log(result)
		})
	}

	useEffect(() =>  {
		fetchUsers()
	}, [])

	return(
    <>
			{	
				(user.isAdmin === true) ?
					<AdminView userData={users} fetchUsers={fetchUsers}/>
          :
          <UserView userData={users} />						
			}
		</>
	)
}