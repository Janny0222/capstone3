import {Container, Col, Row, Card} from 'react-bootstrap';
import React, { useState, useEffect } from 'react';
import { ClipLoader } from 'react-spinners';
import {NavLink} from 'react-router-dom'
import Catalog1 from '../assets/img/catalog-mobile.png';
import Catalog2 from '../assets/img/catalog-pc.png';
import Catalog3 from '../assets/img/catalog-applewatch.png';
import Catalog5 from '../assets/img/catalog-airpods.png';
import Catalog4 from '../assets/img/catalog-ipad.png';
import Catalog6 from '../assets/img/catalog-accessories.png';

export default function Catalog(){
const [isLoading, setIsLoading] = useState(true)
const [products, setProducts] = useState([])

useEffect(() => {
  const fetchProducts = () => {
		fetch(`${process.env.REACT_APP_API_URL}/api/products/allProducts`)
		.then(response => response.json())
		.then(result => {
			setProducts(result)
		})
	}
  setTimeout(() => {
    setIsLoading(false);
  }, 2000)
}, [])

  

	return(
		<>
    
			<Container className='p-3'>
      <h1 className='py-3'>Product Catalog</h1>
      {isLoading ? (
        <div>
        <p className='loading'>Loading please wait...</p>
          <ClipLoader size={150} color={'#343a40'} loading={isLoading} />
        </div>
      ) : (
        <Row>
          <Col lg={4} md={6} sm={12}>
            <div className='catalog-product py-3'>
              <Card className=''>
                <Card.Img variant="top" src={Catalog1} />
                <Card.Body>
                <hr/>
                    <Card.Title className='catalog-product-mobile ' as={NavLink} to={'/catalog/mobile'}>iPhone</Card.Title>
                    <Card.Text>
                    The iPhone 14 display has rounded corners that follow a beautiful curved design, and these corners are within a standard rectangle
                    </Card.Text>
                </Card.Body>
              </Card>
            </div>
          </Col>
          

          <Col lg={4} md={6} sm={12}>
            <div className='catalog-product py-3'>
              <Card className=''>
                <Card.Img variant="top" src={Catalog2} />
                <Card.Body>
                <hr/>
                    <Card.Title className='catalog-product-pc' as={NavLink} to={'/catalog/pc'}>MacPro</Card.Title>
                    <Card.Text>
                    Mac Pro is a series of workstations and servers for professionals made by Apple Inc. since 2006. The Mac Pro, by some performance benchmarks, is the most powerful computer that Apple offers.
                    </Card.Text>
                </Card.Body>
              </Card>
            </div>
          </Col>

          <Col lg={4} md={6} sm={12}>
            <div className='catalog-product py-3'>
              <Card className=''>
                <Card.Img variant="top" src={Catalog3} />
                <Card.Body>
                <hr/>
                    <Card.Title className='catalog-product-watch' as={NavLink} to={'/catalog/watch'}>Watch</Card.Title>
                    <Card.Text>
                    Apple Watch is a wearable smartwatch that allows users to accomplish a variety of tasks, including making phone calls, sending text messages and reading email
                    </Card.Text>
                </Card.Body>
              </Card>
            </div>
          </Col>

          <Col lg={4} md={6} sm={12}>
            <div className='catalog-product py-3'>
              <Card className=''>
                <Card.Img variant="top" src={Catalog4} />
                <Card.Body>
                <hr/>
                    <Card.Title className='catalog-product-tablet' as={NavLink} to={'/catalog/tablet'}>iPad</Card.Title>
                    <Card.Text>
                    The iPad is a brand of iOS and iPadOS-based tablet computers that are developed by Apple Inc, first introduced on January 27, 2010.
                    </Card.Text>
                </Card.Body>
              </Card>
            </div>
          </Col>

          <Col lg={4} md={6} sm={12}>
            <div className='catalog-product py-3'>
              <Card className=''>
                <Card.Img variant="top" src={Catalog5} />
                <Card.Body>
                <hr/>
                    <Card.Title className='catalog-product-airpods' as={NavLink} to={'/catalog/airpods'}>AirPods</Card.Title>
                    <Card.Text>
                    AirPods deliver the wireless headphone experience, reimagined. Just pull them out of the Lightning Charging Case and they're ready to use with your iPhone, Apple Watch, iPad, or Mac.
                    </Card.Text>
                </Card.Body>
              </Card>
            </div>
          </Col>

          <Col lg={4} md={6} sm={12}>
            <div className='catalog-product py-3'>
              <Card className='catalog-product'>
                <Card.Img variant="top" src={Catalog6} />
                <Card.Body>
                <hr/>
                  <Card.Title className='catalog-product-accessories' as={NavLink} to={'/catalog/accessories'}>Accessories</Card.Title>
                    <Card.Text>
                    ccessories are items of equipment that are not usually essential, but which can be used with or added to something else in order to make it more efficient, useful, or decorative.
                    </Card.Text>
                </Card.Body>
              </Card>
            </div>
          </Col>
        </Row>

        )}
      </Container>
		</>
	)
}