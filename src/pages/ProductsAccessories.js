import { useContext, useEffect, useState } from 'react';
import AdminView from '../components/AdminView.js'
import UserView from '../components/UserView.js'
import CatalogAccessories from '../components/CatalogAccessories.js'
import UserContext from '../UserContext.js';
import Catalog from './Catalog.js';

export default function ProductAccessories(){
	const [products, setProducts] = useState([])
	const {user} = useContext(UserContext)

	const fetchProducts = () => {
		fetch(`${process.env.REACT_APP_API_URL}/api/products/allProducts`)
		.then(response => response.json())
		.then(result => {
			setProducts(result)
		})
	}

	useEffect(() =>  {
		fetchProducts()
	}, [])

	return(
  
   
        <CatalogAccessories catalogsData={products} />
   
	)
}