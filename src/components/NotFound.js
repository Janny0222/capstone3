import React from "react";

export default function NotFound(){
  return(
    <div className="text-center pageNotFound">
    <h1>Oops!</h1>
    <h5>404 - Page Not Found</h5>
    <p>The page you are looking for might have been removed or is temporarirly unavailable</p>
    </div>
  )
}