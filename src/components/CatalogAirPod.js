import {useState, useEffect} from 'react';
import {Button, Container, Row, Col} from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons';
import { ClipLoader } from 'react-spinners';
import ProductList from './ProductList.js'


export default function CatalogAirPod({catalogsData}){
  const [product, setProducts] = useState([])
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
  if(catalogsData && catalogsData.length > 0) {
    const active_products = catalogsData.map(product => {
      if(product.isActive == true && product.catalog === 'AirPods'){
        return (
                <ProductList product={product} key={product._id} /> 
          ) 
        }else {
          return null 
        }
    })
    setProducts(active_products)
  }
    setTimeout(()=> {
      setIsLoading(false)
    }, 2000)
  }, [catalogsData]);

  const handleGoBack = () => {
    window.history.back();
  };
  return(
    <>
      <Container>
        <Row>
        <div className='pt-5'>
          <Button variant="dark" onClick={handleGoBack}>
          <FontAwesomeIcon icon={faArrowLeft} /> Go Back
          </Button>
        </div>
				<h1>Our AirPod's</h1>
        {isLoading ? ( 
      <div>
      <p className='loading'>Loading our products, please wait</p>
      <ClipLoader size={150} color={'#343a40'} isLoading={isLoading}/>
      </div>
        ):(
          <Col lg={6} md={12} sm={12}>
            { product }
          </Col>
          )}
        </Row>
      </Container>
		</>
  )
}
