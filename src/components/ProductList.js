import { Card, Col} from 'react-bootstrap';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom'

export default function ProductList({product}){

	const {_id, productName, description, quantity, price} = product;
	

	return (
			<Col >
				<Card className="my-2">
					<Card.Body >
						<Card.Title>{productName}</Card.Title>
						<Card.Subtitle>Descriptions:</Card.Subtitle>
						<Card.Text>{description}</Card.Text>
				
								<Card.Subtitle>Quantity</Card.Subtitle>
						<Card.Text>{quantity}</Card.Text>
				
						<Card.Subtitle>Price:</Card.Subtitle>
						<Card.Text>PhP{price}</Card.Text>
						<Link className="btn btn-success" to={`/products/${_id}`}>Click to Order</Link>
					</Card.Body>
				</Card>
			</Col>
	)
}
	

ProductList.propTypes = {
	product: PropTypes.shape({
		productName: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		quantity: PropTypes.number.isRequired,
		price: PropTypes.number.isRequired,
	})
}