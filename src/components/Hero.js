import {Button, Row, Col} from 'react-bootstrap'
import {useState, useEffect} from 'react'

export default function Banner(){
	const[currentDateTime, setCurrentDateTime] = useState(new Date())

	useEffect(() => {
		const interval = setInterval(() => {
			setCurrentDateTime(new Date());
		}, 1000);
		return () => {
			clearInterval(interval)
		};
	}, [])
	const days = {weekday: 'long', year: 'numeric', month: 'long', day: 'numeric', hour: 'numeric', minute: 'numeric', second: 'numeric'};
	const hour = currentDateTime.getHours();
	const formattedDateTime = currentDateTime.toLocaleString(undefined, days);
	
	let greetings ='';
	
		if(hour >= 5 && hour < 12 ){
			greetings= "Good Morning!";
		} else if (hour >= 12 && hour < 18){
			greetings= "Good Afternoon!";
		} else {
			greetings = "Good Evening!";
		}
	
	
	return (
		<>
		<div className='text-end homeDate'><span >{formattedDateTime}</span></div>
		<div className="text-end homeGreetings"><h3 className='font-italic'>{greetings}</h3></div>
		<Row>
			<Col className="p-5 text-center">
				
				<h1>Jan's Online Shop </h1>
				<p>Everyone has an equal chance of doing business</p>
				<Button variant="success" href="/catalog">
					Order Now!
				</Button>
			</Col>
		</Row>
	</>
	)
}