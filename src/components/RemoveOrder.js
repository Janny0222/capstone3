import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function RemoveOrder({order_id, fetchOrder, fetchCartData}){

  const remove=() => {
    fetch(`${process.env.REACT_APP_API_URL}/api/orders/${order_id}/remove`, {
      method: "DELETE",
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(response => response.json())
    .then(result => {
      console.log(result);
      if (result) {
        Swal.fire({
          title: "Order Deleted",
          text: `The order successfully Activated`,
          icon: 'success'
        });
        fetchCartData();
        fetchOrder();
      } else {
        Swal.fire({
          title: "Error",
          text: "An error occurred while activating the course.",
          icon: 'error'
        });
      }
    });
  }

  return(
  <>
    <Button variant="danger" onClick={remove}>Delete</Button>
  </>
  )
}