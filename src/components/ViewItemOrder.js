import {useState, useEffect} from 'react';
import {Button, Modal, ModalBody, ModalHeader, Form} from 'react-bootstrap';


export default function ViewItemOrder({order_id, productsList, fetchOrder}){
  const [viewModal, setViewModal] = useState(false)
  const [quantity, setQuantity] = useState(Array(productsList.length).fill(0));

  const handleLoginModalToggle = () => {
		setViewModal(!viewModal)
  }
  useEffect(() => {
    fetchOrder()
  }, [])
  

  return(
    <>
    <Button variant="secondary" onClick={handleLoginModalToggle}>
        View
      </Button>
    <Modal show={viewModal} onHide={handleLoginModalToggle}>
    <ModalHeader closeButton></ModalHeader>
    <ModalBody>
    <Form onSubmit={event => (event)}>
            {productsList.length > 0 ? (
              productsList.map((product, index) => (
                <div key={index}>
                  <Form.Group controlId={`productName${index}`}>
                    <Form.Label>Product Name</Form.Label>
                    <Form.Control
                      type="text"
                      required
                      value={product.productName}
                    />
                  </Form.Group>
                  <Form.Group controlId={`productName${index}`}>
                    <Form.Label>Quantity</Form.Label>
                    <Form.Control
                      type="text"
                      required
                      value={product.quantity} 
                      onChange={event => {setQuantity(event.target.value)}}
                    />
                  </Form.Group>
                  <Button variant="secondary" className='m-1'>Update</Button>
                </div>
              ))
            ) : (
              <tr>
                <td colSpan="2">No products available</td>
              </tr>
            )}
          </Form>
    </ModalBody>
    </Modal>
    </>
  )
}