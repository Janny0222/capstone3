import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function ArchiveActivate({product_id, fetchProducts, isActive}){
  const archive = () =>{
    fetch(`${process.env.REACT_APP_API_URL}/api/products/${product_id}/archive`, {
      method: "PUT",
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(response => response.json())
    .then(result => {
      if (result) {
        Swal.fire({
          title: "Product Archived",
          text: `The Product successfully Archived`,
          icon: 'success'
        });
        fetchProducts();
      } else {
        Swal.fire({
          title: "Error",
          text: "An error occurred while archiving the product.",
          icon: 'error'
        });
      }
    });
  }

  const activate = () => {
    fetch(`${process.env.REACT_APP_API_URL}/api/products/${product_id}/activate`, {
      method: "PUT",
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(response => response.json())
    .then(result => {
      console.log(result);
      if (result) {
        Swal.fire({
          title: "Product Activated",
          text: `The Product successfully Activated`,
          icon: 'success'
        });
        fetchProducts();
      } else {
        Swal.fire({
          title: "Error",
          text: "An error occurred while activating the product.",
          icon: 'error'
        });
      }
    });
  }

  return(
  <>
    { isActive ?
    <Button variant="warning" onClick={archive}>Archive</Button>
    :
    <Button variant="success" onClick={activate}>Activate</Button>
    }
  </>
  )
}