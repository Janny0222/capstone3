import { Card} from 'react-bootstrap';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom'

export default function OrderList({order}){

	const {_id, userId, fullName, quantity, totalAmount} = order;


	return (
		<Card className="my-2">
			<Card.Body >
				<Card.Title>{userId}</Card.Title>
				<Card.Subtitle>Full Name:</Card.Subtitle>
				<Card.Text>{fullName}</Card.Text>

        <Card.Subtitle>Quantity</Card.Subtitle>
				<Card.Text>{quantity}</Card.Text>
						
				<Card.Subtitle>Total Amount</Card.Subtitle>
				<Card.Text>PhP{totalAmount}</Card.Text>

			</Card.Body>
		</Card>

	)

}
	

OrderList.propTypes = {
	order: PropTypes.shape({
		userId: PropTypes.string.isRequired,
		fullName: PropTypes.string.isRequired,
    products: [{
      productId: PropTypes.string.isRequired,
      quantity: PropTypes.number.isRequired
    }],
		totalAmount: PropTypes.number.isRequired,
	})
}