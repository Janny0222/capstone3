import {useState, useEffect} from 'react';
import {Table, Button, Dropdown, ButtonGroup} from 'react-bootstrap';
import EditProduct from './EditProduct';
import ArchiveActivate from './ArchiveActivate';


export default function AdminView({productsData, fetchProducts}){


  const [products, setProducts] = useState([]);
  const [orders, setOrders] = useState([]);
  const [users, setUsers] = useState([]);
  const [view, setView] = useState('products');
  const [screenWidth, setScreenWidth] = useState(window.innerWidth);

  let token = localStorage.getItem('token');

  const handleResize = () => {
    setScreenWidth(window.innerWidth);
  };

  useEffect(() => {
    window.addEventListener('resize', handleResize);
    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []);

  const fetchUsers = () => {
		fetch(`${process.env.REACT_APP_API_URL}/api/users/AllUser`,{
      method: 'GET',
      headers:{
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`
      }
    })
		.then(response => response.json())
		.then(result => {
			setUsers(result)
      console.log(result)
		})
	}

	useEffect(() =>  {
		fetchUsers()
	}, [])

  const renderUser = () => {
    return users.map(user => (
      <tr key={user._id}>
        <td>{user._id}</td>
        <td>{user.firstName} {user.lastName}</td>
        <td>{user.age}</td>
        <td>{user.email}</td>
        <td>{user.password}</td>
        <td>{user.mobileNo}</td>
        <td>{user.isAdmin ? 'Admin' : 'User'}</td>
      </tr>
    ))
  }

  useEffect(() => {
    const product_array = productsData.map(product => {
      return (
        <tr  key={product._id}>
          <td>{product._id}</td>
          <td>{product.productName}</td>
          <td>{product.description}</td>
          <td>{product.quantity}</td>
          <td>{product.price}</td>
          <td>{product.catalog}</td>
          <td>{product.isActive ? 'Available' : 'Unvailable'}</td>
          <td>
            <EditProduct 
            product_id={product._id} fetchProducts={fetchProducts}/>
            
          </td>
            <td>
            <ArchiveActivate 
            product_id={product._id} 
            fetchProducts={fetchProducts} 
            isActive={product.isActive}/>
            </td>
        </tr>
      )
    })
    setProducts(product_array)
  }, [productsData, fetchProducts]);


  const getAllOrder = () => {
  fetch(`${process.env.REACT_APP_API_URL}/api/orders/all`,{
    method: 'GET',
    headers: {
    'Content-Type': 'application/json',
    Authorization: `Bearer ${token}`
  }
  }).then(response =>  response.json()).then(result => {
    setOrders(result)
  })
}
useEffect(() => {
  getAllOrder();
}, []);

  const renderOrder = () => {
    return orders.map(order => (
      order.products.map(prod => (
      <tr key={order._id}>
        <td>{order._id}</td>
        <td>{order.fullName}</td>
        <td>{prod.productName}</td>
        <td>{prod.quantity}</td>
        <td>{order.totalAmount}</td>
        <td>{order.isPaid ? 'Paid' : 'Not Paid'}</td>
      </tr>
      ))
    ))
  }

  const renderView = () => {
    if (view === 'products') {
      return (
        <>
        <h2> Apple Product's </h2>
        <Table striped bordered hover responsive>
          <thead>
            <tr className='text-center'>
              <th>ID</th>
              <th>Name</th>
              <th>Description</th>
              <th>Quantity</th>
              <th>Price</th>
              <th>Catalog</th>
              <th>Availability</th>
              <th colSpan = '2'>Actions</th>
            </tr>
          </thead>
          <tbody>
            {products}
          </tbody>
        </Table>
        </>
      );
    } else if (view === 'users') {
      return (
        <>
        <h2> User's Information </h2>
        <Table striped bordered hover responsive>
          <thead>
            <tr className='text-center'>
              <th>User ID</th>
              <th>Full Name</th>
              <th>Age</th>
              <th>Email</th>
              <th>Password</th>
              <th>Mobile Number</th>
              <th>Previledge</th>
            </tr>
          </thead>
          <tbody>
            {renderUser()}
          </tbody>
        </Table>
        </>
      );
      return <div>Users View</div>;
    } else if (view === 'orders') {
      return (
        <>
        <h2> All User's Order </h2>
        <Table striped bordered hover responsive>
          <thead>
            <tr className='text-center'>
              <th>Order ID</th>
              <th>Full Name</th>
              <th>Product Name</th>
              <th>Quantity</th>
              <th>Total Amount</th>
              <th>Status</th>
            </tr>
          </thead>
          <tbody>
            {renderOrder()}
          </tbody>
        </Table>
        </>
      );
    }
  };

  return(
    <>
      <div className="">
        <h1> Admin Dashboard</h1>
        
        <div className="view-buttons">
          {screenWidth <= 390 ? (
          <Dropdown as={ButtonGroup}>
            <Dropdown.Toggle split variant="dark" id="dropdown-split-basic">
            {view === 'products' ? 'Products' : view === 'users' ? 'Users' : 'Orders'}
            </Dropdown.Toggle>
            <div className='bg-dark'>
              <Dropdown.Menu className='dark'>
                <Dropdown.Item
                className=''
                  active={view === 'products'}
                  onClick={() => setView('products')}>
                  Products
                  </Dropdown.Item>
                  <Dropdown.Item
                  className=''
                  active={view === 'users'}
                  onClick={() => setView('users')}>
                  Users
                  </Dropdown.Item>
                  <Dropdown.Item
                  className=''
                  active={view === 'orders'}
                  onClick={() => setView('orders')}>
                  Orders
                  </Dropdown.Item>
              </Dropdown.Menu>
            </div>
          </Dropdown>
          ) : (
            <>
          <Button className='me-3' variant={view === 'products' ? 'primary' : 'secondary'} onClick={() => setView('products')}>
            Products
          </Button>
          <Button className='me-3' variant={view === 'users' ? 'primary' : 'secondary'} onClick={() => setView('users')}>
            Users
          </Button>
          <Button variant={view === 'orders' ? 'primary' : 'secondary'} onClick={() => setView('orders')}>
            Orders
          </Button>
          </>
          )}
        </div>
        {renderView()}
      </div>
    </>

    
  )
}