import { Row, Col} from 'react-bootstrap'
import heroImage from '../assets/img/iphone14-pro-max-lineup.png';
import heroImage2 from '../assets/img/MacProTower.png';
import heroImage3 from '../assets/img/watch.png';
import heroImage4 from '../assets/img/iPad.png';
import heroImage5 from '../assets/img/airpods.png';
import heroImage6 from '../assets/img/accessories.png';

export default function Highlights(){
	return(
		<>
		<h4></h4>
		<Row className="my-3">
			<Col xs={12} lg={4} md={6} className="mt-2">
							<div className='hero-page'>
								<h2 className='text-dark overlay-text'>Mobile</h2>
								<div className='img-hero'><img src={heroImage} alt="Hero" className='img-fluid'></img></div>
							</div>
			</Col>
			<Col xs={12} lg={4} md={6} className="mt-2">
							<div className='hero-page'>
								<h2 className='text-dark overlay-text'>PC</h2>
								<div className='img-hero'><img src={heroImage2} alt="Hero" className='img-fluid'></img></div>
							</div>
			</Col>
			<Col xs={12} lg={4} md={6} className="mt-2">
							<div className='hero-page'>
								<h2 className='text-dark overlay-text'>Watch</h2>
								<div className='img-hero'><img src={heroImage3} alt="Hero" className='img-fluid'></img></div>
							</div>
			</Col>
			<Col xs={12} lg={4} md={6} className="mt-2">
							<div className='hero-page'>
								<h2 className='text-dark overlay-text'>Tablet</h2>
								<div className='img-hero'><img src={heroImage4} alt="Hero" className='img-fluid'></img></div>
							</div>
			</Col>
			<Col xs={12} lg={4} md={6} className="mt-2">
							<div className='hero-page'>
								<h2 className='text-dark overlay-text'>AirPods</h2>
								<div className='img-hero'><img src={heroImage5} alt="Hero" className='img-fluid'></img></div>
							</div>
			</Col>
			<Col xs={12} lg={4} md={6} className="mt-2">
							<div className='hero-page'>
								<h2 className='text-dark overlay-text'>Accessories</h2>
								<div className='img-hero'><img src={heroImage6} alt="Hero" className='img-fluid'></img></div>
							</div>
			</Col>
		</Row>
		</>
	)
}