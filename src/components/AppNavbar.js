import { Navbar, Nav, Container, NavDropdown, Modal, Button, Form } from 'react-bootstrap';
import { Link, NavLink, Navigate } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUser, faShoppingCart, faArrowRight, faArrowLeft } from '@fortawesome/free-solid-svg-icons';
import {useContext, useState, useEffect} from 'react';
import UserContext from '../UserContext';
import { useCart } from '../CartContext.js'
import Swal from 'sweetalert2'

export default function AppNavbar(){
  const [showRegisterForm, setShowRegisterForm] = useState(false);
	const [showLogin, setShowLogin] = useState(false)
	const [profileName, setProfileName] = useState(false)
  const [FirstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [age, setAge] = useState("");
	const [mobileNo, setMobileNo] = useState("");
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [confirmPassword, setConfirmPassword] = useState("");
	
	const {user, setUser} = useContext(UserContext);
	const [emailLogin, setEmailLogin] = useState("");
	const [passwordLogin, setPasswordLogin] = useState("");
	const [isActve, setIsActive] = useState(false);

	const [step, setStep] = useState(1);

	const {firstName} = user;
	const { cartItemCount, setCartItemCount } = useCart();
	//const [ cartItemCounts, setCartItemCounts ] = useState(0);
	console.log(cartItemCount);
	//console.log(cartItemCounts)


	// For Cart
	const fetchCartData = () => {
		fetch(`${process.env.REACT_APP_API_URL}/api/orders`, {
			method: 'GET',
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		}).then(response => response.json()).then(result => {
			console.log(result)
			const itemCount = result.filter(order => !order.isPaid).length;
			setCartItemCount(itemCount)
			console.log(itemCount)
		}).catch(error => {
			console.error('Error Fetching Cart Data', error)
		});
	};
	useEffect(() => {
		fetchCartData();
	}, []);
	//for register modal
  const handleRegisterModalToggle = () => {
    setShowRegisterForm(!showRegisterForm) 
    setFirstName("")
		setLastName("")
    setAge("")
		setEmail("")
		setMobileNo("")
		setPassword("")
		setConfirmPassword("")
  }

	//for login modal
	const handleLoginModalToggle = () => {
		setShowLogin(!showLogin)
	}
	// For Register form
	const handleNext = () => {
    if (step === 1) {
      setStep(2); // 
    }
  };

	const handleBack = () => {
    if (step === 2) {
      setStep(1);
    }
  }

  function registerUser (event) {
		// Prevents page load upon form submission
		event.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/api/users/register`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				firstName: FirstName,
				lastName: lastName,
        age: age,
				email: email,
				mobileNo: mobileNo,
				password: password
			})
		}).then(response => response.json()).then(result => {
			if(result){
				// To reset all the fields if successfully register
				setFirstName("")
				setLastName("")
        setAge("")
				setEmail("")
				setMobileNo("")
				setPassword("")
				setConfirmPassword("");

				setShowRegisterForm(false)

				Swal.fire({
					title: 'Congratulations!',
					text: `Successfully register a user`,
					icon: 'success'
				})

        
			} else {
				Swal.fire({
				title: 'Something went wrong!',
				text: `Please try again`,
				icon: 'error'
				})
			}
		})
	} 
	useEffect(() => {
		if((FirstName !== "" && lastName !== "" && email !== "" && age !== "" && mobileNo !== "" && password !== "" && confirmPassword !== "") && (password === confirmPassword) && (mobileNo.length === 11)) {
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [FirstName, lastName, email, age, mobileNo, password, confirmPassword]);

	function loginUser (event) {
		event.preventDefault();
		fetch(`${process.env.REACT_APP_API_URL}/api/users/login`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: emailLogin,
				password: passwordLogin
			})
		}).then(response => response.json()).then( result => {
			console.log(result)
			if(result.accessToken){
				console.log(result)
				localStorage.setItem('token', result.accessToken);
				localStorage.setItem('userId', result.userId);
				localStorage.setItem('isAdmin', result.isAdmin);
			  localStorage.setItem('firstName', result.firstName);
				
				retrieveUserDetails(result.accessToken, result.userId)

				setEmailLogin("")
				setPasswordLogin("")
				setShowLogin(false);
				setProfileName(result.firstName);

				fetchCartData();
				Swal.fire({
					title: 'Login Success',
					text: 'You have logged in successfully!',
					icon: 'success'
				})
				
			}else {
				Swal.fire({
					title: 'Something went wrong',
					text: `${email} does not exist`,
					icon: 'warning'
				})
			}
		})
	} 
	const retrieveUserDetails = (token, userId) => {
	fetch(`${process.env.REACT_APP_API_URL}/api/users/userProfile`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${token}`
			},
			body: JSON.stringify({
				id: userId
			})
		}).then(response => response.json()).then( result => {
			console.log(result)
			setUser({
				id: result._id,
				isAdmin: result.isAdmin
			})
		})
	}
	useEffect(() => {
		if(emailLogin !== "" && passwordLogin !== "") {
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [emailLogin, passwordLogin]);

	const TypingText = ({ text }) => {
		const [displayText, setDisplayText] = useState('');
	
		useEffect(() => {
			let currentIndex = 0;
			const interval = setInterval(() => {
				if (currentIndex <= text.length) {
					setDisplayText(text.slice(0, currentIndex));
					currentIndex++;
				} else {
					clearInterval(interval);
				}
			}, 100); // Adjust typing speed here (in milliseconds)
	
			return () => {
				clearInterval(interval);
			};
		}, [text]);
	
		return <h3 className="m-0 py-3">{displayText}</h3>;
	};

	return(
    <>
    <Modal show={showRegisterForm} onHide={handleRegisterModalToggle} dialogClassName="custom-modal">
      <Modal.Header closeButton className='p-1 text-danger'></Modal.Header>
        <Modal.Body className='pt-0 bg-dark text-white'>
        <Form className="" onSubmit={(event) => registerUser(event)}>
				<div className='my-4'>
					<h1 className="m-0">Create an account</h1>
					<p className='m-0'> Start to add our products to your cart 😁</p>
				</div>
				{step === 1 && (
					<>
						<Form.Group>
								<Form.Label className='m-0'>First Name:</Form.Label>
								<Form.Control 
									type="text" 
									placeholder="Enter First Name" 
									required
									value={FirstName}
									onChange={event => {setFirstName(event.target.value)}}
									/>
						</Form.Group>
						<Form.Group>
								<Form.Label className='m-0'>Last Name:</Form.Label>
								<Form.Control 
									type="text" 
									placeholder="Enter Last Name" 
									required
									value={lastName}
									onChange={event => {setLastName(event.target.value)}}
									/>
						</Form.Group>
            <Form.Group>
								<Form.Label className='m-0'>Age:</Form.Label>
								<Form.Control 
									type="number" 
									placeholder="Enter Your age" 
									required
									value={age}
									onChange={event => {setAge(event.target.value)}}
									/>
						</Form.Group>
            <Form.Group>
								<Form.Label className='m-0'>Mobile No:</Form.Label>
								<Form.Control 
									type="number" 
									placeholder="Enter 11 Digit No." 
									required
									value={mobileNo}
									onChange={event => {setMobileNo(event.target.value)}}
									/>
						</Form.Group>
						<Button className="my-2" variant="primary" onClick={handleNext}>
            <FontAwesomeIcon icon={faArrowRight}/> Next
          	</Button>
					</>
					)}
					{step === 2 && (
					<>
						<Form.Group>
								<Form.Label className='m-0'>Email:</Form.Label>
								<Form.Control 
									type="email" 
									placeholder="Enter Email" 
									required
									value={email}
									onChange={event => {setEmail(event.target.value)}}
									/>
						</Form.Group>
						<Form.Group>
								<Form.Label className='m-0'>Password:</Form.Label>
								<Form.Control 
									type="password" 
									placeholder="Enter Password" 
									required
									value={password}
									onChange={event => {setPassword(event.target.value)}}
									/>
						</Form.Group>
						<Form.Group>
								<Form.Label className='m-0'>Confirm Password:</Form.Label>
								<Form.Control 
									type="password" 
									placeholder="Confirm Password" 
									required
									value={confirmPassword}
									onChange={event => {setConfirmPassword(event.target.value)}}
									/>
						</Form.Group>
						
						<div className='d-flex justify-content-between'>
							<Button className="my-2" variant="primary" onClick={handleBack}>
													<FontAwesomeIcon icon={faArrowLeft}/> Back
													</Button>
							<Button className="my-2" variant="primary" type="submit" disabled={isActve === false}>Submit
													<Navigate to = '/'/>
													</Button>
						</div>
						</>
						)}    
				</Form>
        </Modal.Body>
    </Modal>
		<Modal show={showLogin} onHide={handleLoginModalToggle}>
			<Modal.Header closeButton className='p-1 bg-light text-danger'></Modal.Header>
			<Modal.Body className='pt-0 bg-dark text-white'>
			<div className='login-box'>
				<Form className="" onSubmit={(event) => loginUser(event)}>
								<h3 className="m-0 py-3">LOGIN:</h3>
										<div className='user-box'>
											<Form.Group>
													<Form.Label className='font-italic'>Email:</Form.Label>
													<Form.Control
														type="text"
														placeholder="Enter your email"
														required
														value={emailLogin}
														onChange={event => {setEmailLogin(event.target.value)}}
														/>
											</Form.Group>
										</div>
										<div className='user-box'>
											<Form.Group>
													<Form.Label className='font-italic'>Password:</Form.Label>
													<Form.Control
														type="password"
														placeholder="Enter your password"
														required
														value={passwordLogin}
														onChange={event => {setPasswordLogin(event.target.value)}}
														/>
											</Form.Group>
										</div>
										<Button className="my-2" variant="primary" type="submit" disabled={isActve === false} onHide={handleLoginModalToggle}>
										Submit</Button>
										<p>Not a member? <Button as={NavLink} to='/' onClick={handleRegisterModalToggle} onHide={handleLoginModalToggle}> Register Now! </Button> </p>
				</Form>
			</div>
			</Modal.Body>
		</Modal>
		<Navbar font="text-white" bg="dark" className="navi-1">
			<Container>
				<Navbar.Brand className="navBar" as={Link} to='/'>JM Store</Navbar.Brand>
					{/* <Navbar.Toggle className="bg-light" aria-controls="basic-navbar-nav"/> */}
					
						<Nav className="ms-auto">
							<Nav.Link className="navBar" as={NavLink} to='/'>Home</Nav.Link>
							
					{ (user.id !== null) ?
						user.isAdmin ?
								<>
									<Nav.Link className="navBar m-0" as={NavLink} to='/products'>Dashboard</Nav.Link>
									<Nav.Link className="navBar m-0" as={NavLink} to='/products/add'>Add Item</Nav.Link>
									<NavDropdown 
									className="getStarted" 
									id="basic-nav-dropdown" 
									title={<FontAwesomeIcon 
									icon={faUser} />}
									align="end"
									>
											<div className='dropdown-val'>
												<div >
													<NavDropdown.Item 
													className="dropdown-val-item" 
													as={NavLink} 
													to='/profile'
													>{profileName || firstName}</NavDropdown.Item>
													<NavDropdown.Item 
													className="dropdown-val-item" 
													as={NavLink} 
													to='/logout'
													>Logout</NavDropdown.Item>
												</div>
											</div>
									</NavDropdown>
								</>
							:
						
								<>
									<Nav.Link className="navBar" as={NavLink} to='/catalog' >Catalog</Nav.Link>
									<Nav.Link className="navBar" as={NavLink} to='/cart'>
              		<FontAwesomeIcon icon={faShoppingCart} />
									<span className="cart-item-count">{cartItemCount}</span>
            			</Nav.Link>
									<NavDropdown 
									className="getStarted" 
									id="basic-nav-dropdown" 
									title={<FontAwesomeIcon 
									icon={faUser} />}
									align="end">
											<div className='dropdown-val'>
												<div >
													<NavDropdown.Item 
													className="dropdown-val-item" 
													as={NavLink} 
													to='/profile'
													>{profileName || firstName}</NavDropdown.Item>
													<NavDropdown.Item 
													className="dropdown-val-item" 
													as={NavLink} 
													to='/logout'
													>Logout</NavDropdown.Item>
												</div>
											</div>
									</NavDropdown>
								</>
						:
						<>
							<Nav.Link className="navBar" as={NavLink} to='/catalog'>Catalog</Nav.Link>
							
							<NavDropdown 
							className="getStarted custom-dropdwown" 
							title="Get Started" 
							id="basic-nav-dropdown"
							align="end">
                <div className='dropdown-val'>
                  <div >
                    <NavDropdown.Item 
                    className="dropdown-val-item" 
                    as={NavLink} 
                    onClick={handleRegisterModalToggle}
                    >Register</NavDropdown.Item>
                    <NavDropdown.Item 
										className="dropdown-val-item" 
										as={NavLink} 
										onClick={handleLoginModalToggle}
										>Login</NavDropdown.Item>
                  </div>
                </div>
              </NavDropdown>
						</>
					}
							
					</Nav>
				
			</Container>
		</Navbar>
    </>
	)
}
