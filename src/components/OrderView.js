import { useState, useEffect, useContext } from 'react';
import { Table, Button, Dropdown, ButtonGroup, Container } from 'react-bootstrap';
import ViewItemOrder from './ViewItemOrder';
import RemoveOrder from './RemoveOrder.js';
import Swal from 'sweetalert2'
import DropdownToggle from 'react-bootstrap/esm/DropdownToggle';

export default function OrderView({ orderedData, fetchOrder, fetchCartData }) {
  const [totalAmount, setTotalAmount] = useState(0);
  const [isEnable, setIsEnable] = useState(false);
  const [view, setView] = useState('current');
  const [orderHistory, setOrderHistory] = useState([])
  const [screenWidth, setScreenWidth] = useState(window.innerWidth);

  let token = localStorage.getItem('token');

  useEffect(() => {
    if (Array.isArray(orderedData)) {
      const unpaidOrders = orderedData.filter(order => !order.isPaid);
      setTotalAmount(
        unpaidOrders.reduce((total, order) => total + order.totalAmount, 0)
      );
      setIsEnable(totalAmount !== 0);
    }
  }, [orderedData, totalAmount]);

  const handleHistory = () => {
    fetch(`${process.env.REACT_APP_API_URL}/api/orders/`, {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${token}`
      }
    }).then(response => response.json()).then(result => {
      const paid_order = result.filter(order => order.isPaid === true);
      setOrderHistory(paid_order)
    }).catch(error => {
      console.error('Error fetching paid orders', error)
    })
  }

  useEffect(() => {
    handleHistory();
  }, [])


  const handleCheckOut = () => {
    fetch(`${process.env.REACT_APP_API_URL}/api/orders/checkout`,{
      method: "PATCH",
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`
      }
    }).then(response =>  response.json()).then(result => {
			if(result.message === 'Payment Successful'){
        
        Swal.fire({
          title: 'Success!!!',
          text: result.message,
          icon: 'success'
        })
        handleHistory();
        fetchCartData();
        fetchOrder();
      }else {
        Swal.fire({
          title: 'Something went wrong!!!',
          text: 'Please try again',
          icon: 'error'
        })
      }
		})
  }

  useEffect(() => {
    const handleResize = () => {
      setScreenWidth(window.innerWidth);
    };
    window.addEventListener("resize", handleResize);
    return () => {
      window.addEventListener("resize", handleResize)
    }
  })

  const order_view = () => {
    if (view === 'current') {
  return (
    <div className="">
      <h4> Pending Order </h4>
      {Array.isArray(orderedData) && orderedData.length > 0 ? (
        <Table striped bordered hover responsive>
          <thead className=''>
            <tr className="text-center order-head">
              <th>User ID</th>
              <th>Full Name</th>
              <th>Product Name</th>
              <th>Quantity</th>
              <th>Total Amount</th>
              <th colSpan="2">Actions</th>
            </tr>
          </thead>
          <tbody>
            {orderedData.map(order =>(
              order.products.map(prod => (
              order.isPaid === false && (
              <tr key={order._id}>
                <td>{order.userId}</td>
                <td>{order.fullName}</td>
                <td>{prod.productName}</td>
                <td>{prod.quantity}</td>
                <td>{order.totalAmount}</td>
                <td>
                  <ViewItemOrder
                    order_id={order._id}
                    productsList={order.products}
                    fetchOrder={fetchOrder}
                  />
                </td>
                <td>
                  <RemoveOrder order_id={order._id} 
                  fetchOrder={fetchOrder} 
                  fetchCartData={fetchCartData}
                  />
                </td>
              </tr>
              )
              ))
            ))}
          </tbody>
        </Table>
      ) : orderedData.length === 0 ?(
        <p>No Orders Found</p>
      ) : (
        <p>No Pending Orders</p>
      )}
      <div className="d-flex justify-content-end ms-2">
        <div className="p-1">
          <label>Total:</label>
        </div>
        <div className="p-1">
          <input
            value={"PhP " + (isEnable ? totalAmount : 0)}
            type="text"
            disabled
          />
        </div>
        <div className="p-1">
          <Button 
          variant="success" 
          className="checkout" 
          disabled={!isEnable}
          onClick={handleCheckOut}>
            Check Out
          </Button>
        </div>
      </div>
    </div>
  );
      } else if (view === 'history') {
        return (
          <>
        {orderHistory.length > 0 ? (
          <div>
            <h4>Order History</h4>
            <Table striped bordered hover responsive>
              <thead>
                <tr className="text-center">
                  <th>Order ID</th>
                  <th>Full Name</th>
                  <th>Product Name</th>
                  <th>Total Amount</th>
                  <th>Date of Purchased</th>
                  <th>Status</th>
                </tr>
              </thead>
              <tbody>
                {orderHistory.map(order => (
                order.products.map(result => (
                  <tr key={order._id}>
                    <td>{order._id}</td>
                    <td>{order.fullName}</td>
                    <td>{result.productName}</td>
                    <td>{order.totalAmount}</td>
                    <td>{order.purchasedOn}</td>
                    <td>{order.isPaid ? 'Ready To Deliver' : 'Payment Issue'}</td>
                  </tr>
                ))
                ))}
              </tbody>
            </Table>
          </div>
        ) : (
          <p>No Paid Orders Found</p>
        )}
        </>
        )
      }
    }
  return (
    <>
    <Container>
      <div>
        <h1>Your Order's</h1>
      <div>
        {screenWidth < 450 ? (
          <Dropdown as={ButtonGroup}>
            <Dropdown.Toggle split variant='dark' id='dropdown-split-basic'>
              {view === 'current' ? 'Current' : 'History'}
            </Dropdown.Toggle>
            <div className='bg-dark'>
              <Dropdown.Menu className='dark'>
                <Dropdown.Item
                active = {view === 'current'}
                onClick={()=>setView('current')}>
                Current </Dropdown.Item>
                <Dropdown.Item
                active = {view === 'history'}
                onClick={()=> setView('history')}>
                History
                </Dropdown.Item>
              </Dropdown.Menu>
            </div>
          </Dropdown>
        ) : (
          <>
            <Button className='me-2' variant={view === 'current' ? 'primary' : 'secondary'} onClick={() => setView('current')}>
              Current
            </Button>
            <Button variant={view === 'history' ? 'primary' : 'secondary'} onClick={() => setView('history')}>
              History
            </Button>
          </>
        )
        }
      </div>
          {order_view()}
      </div>
    </Container>
    </>
  )
}
