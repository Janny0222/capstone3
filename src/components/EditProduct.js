import { Button, Form, Modal } from 'react-bootstrap';
import { useState } from 'react';
import Swal from 'sweetalert2';


export default function EditProduct({product_id, fetchProducts}){
  const [productId, setProductId] = useState("")

  // Form states

  const [productName, setProductName] = useState('')
  const [description, setDescription] = useState('')
  const [quantity, setQuantity] = useState('')
  const [price, setPrice] = useState('')
  const [catalog, setCatalog] = useState('')

  // For modal activation
  const [showEditModal, setShowEditModal] = useState(false);

  const openEditModal = (productId) => {
    fetch(`${process.env.REACT_APP_API_URL}/api/products/${productId}`)
    .then(response => response.json())
    .then(result => {
      // Pre-populate the form input fields with data from the API
      setProductId(result._id)
      setProductName(result.productName)
      setDescription(result.description)
      setQuantity(result.quantity)
      setPrice(result.price)
      setCatalog(result.catalog)

      
      setShowEditModal(true)
    });
  }
  const closeEditModal = () => {
      setShowEditModal(false)
      setProductName('')
      setDescription('')
      setQuantity('')
      setPrice('')
      setCatalog('')
  }

  const editProduct = (event, productId) => {
    event.preventDefault();
    fetch(`${process.env.REACT_APP_API_URL}/api/products/${productId}`,{
      method: "PUT",
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({
        productName : productName,
        description: description,
        quantity: quantity,
        price: price,
        catalog: catalog,
      })
    }).then(response => response.json()).then(result => {
      if(result){
        Swal.fire({
          title: "Product Updated",
          text: `Successfully update the product ${productName}`,
          icon: 'success'
        })
        fetchProducts();
        closeEditModal();
      }else {
        Swal.fire({
          title: "Something Went Wrong",
          text: "Please try again",
          icon: 'error'
        })
        fetchProducts();
        closeEditModal();
      }
    });
  };
  
  return(
    <>
    <Button variant="secondary" onClick={() => openEditModal(product_id)}>Edit</Button>
    {/* // Edit Modal  */}
    <Modal show={showEditModal} onHide={closeEditModal}>
      <form onSubmit={event => editProduct(event, productId)}> 
        <Modal.Header closeButton>
          <Modal.Title>Edit Product</Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <Form.Group controlId="productName">
                <Form.Label>Name</Form.Label>
                <Form.Control 
                type="text" 
                required
                value={productName}
								onChange={event => {setProductName(event.target.value)}}
                />
            </Form.Group>
            <Form.Group controlId="productDescription">
                <Form.Label>Description</Form.Label>
                <Form.Control 
                type="text" 
                required
                value={description}
								onChange={event => {setDescription(event.target.value)}}
                />
            </Form.Group>
            <Form.Group controlId="productQuantity">
                <Form.Label>Quantity</Form.Label>
                <Form.Control 
                type="number" 
                required
                value={quantity}
								onChange={event => {setQuantity(event.target.value)}}
                />
            </Form.Group>
            <Form.Group controlId="productPrice">
                <Form.Label>Price</Form.Label>
                <Form.Control 
                type="number" 
                required
                value={price}
								onChange={event => {setPrice(event.target.value)}}
                />
            </Form.Group>
            <Form.Group controlId="courseCatalog">
                <Form.Label>Catalog</Form.Label>
                <Form.Control 
                type="text" 
                required
                value={catalog}
								onChange={event => {setCatalog(event.target.value)}}
                />
            </Form.Group>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={closeEditModal}>Close</Button>
          <Button variant="success" type='submit'>Submit</Button>
        </Modal.Footer>
      </form>
    </Modal>
    </>
  )
}