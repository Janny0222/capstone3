import {useState, useEffect} from 'react';
import ProductList from './ProductList.js'
import PropTypes from 'prop-types'

export default function UserView({productsData}){
  const [product, setProducts] = useState([])

  useEffect(() => {
    const active_products = productsData.map(product => {
      if(product.isActive == true){
        return (
          <ProductList product={product} key={product._id} />
          ) 
        }else {
          return null
        }
    })
    setProducts(active_products)
  }, [productsData])

  return(
    <>
				<h1>Catalog</h1>
				{ product }
		</>
  )
}